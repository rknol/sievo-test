from setuptools import setup, find_packages

requires = [
    "appdirs==1.4.2",
    "click==6.7",
    "packaging==16.8",
    "py==1.4.32",
    "pyparsing==2.1.10",
    "pytest==3.0.6",
    "six==1.10.0",
]

setup(name='Sievo Programming Assignment',
      version='0.1',
      description='Programming assignment for the Sievo recruitment process',
      author='Ruben Knol',
      author_email='c.minor6@gmail.com',
      license='BSD 3-clause',
      packages=find_packages(),
      install_requires=requires,
      setup_requires=['pytest-runner'],
      tests_require=requires,
      entry_points='''
        [console_scripts]
        command.py=sievo.command:app
    ''',
      zip_safe=False)