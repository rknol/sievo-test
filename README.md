# Sievo Programming Assignment

You're looking at my implementation of the programming test given by Sievo. I've timeboxed myself to work on this for
a maximum of 4 hours, and finished the implementation in just under 4 hours and spent a little bit extra time on
simple unit tests and this readme file so that you can understand the mechanics and considerations made
for this implementation.

## Requirements

This application requires the following:

* Python 3.4 or Python 3.5
* pip3

## Installation

I would recommend you use virtualenv so that you don't pollute your global Python's dependencies:

```
$ virtualenv ./venv && source ./venv/bin/activate
```

To install the application, use setup.py:

```
(venv) $ python setup.py install
```

## Usage

The application comes with a manual:

```
(venv) $ command.py --help

Usage: command.py [OPTIONS] FILE

Options:
  --project TEXT        Filter results by project
  --sort-by-start-date  Sort results by start date
  --help                Show this message and exit.
```

Some example usage (based on the example file provided):

```
(venv) $ command.py ./test.txt
Project	Description	Start date	Category	Responsible	Savings amount	Currency	Complexity
2	Harmonize Lactobacillus acidophilus sourcing	2014-01-01 00:00:00.000	Dairy	Daisy Milks	NULL	NULL	Simple
3	Substitute Crème fraîche with evaporated milk in ice-cream products	2013-01-01 00:00:00.000	Dairy	Daisy Milks	141415.942696	EUR	Moderate
3	Substitute Crème fraîche with evaporated milk in ice-cream products	2013-01-01 00:00:00.000	Dairy	Daisy Milks	141415.942696	EUR	Moderate
4	Decrease production related non-categorized side costs	2013-01-01 00:00:00.000	Dairy	Daisy Milks	11689.322459	EUR	Hazardous
4	Decrease production related non-categorized side costs	2013-01-01 00:00:00.000	Dairy	Daisy Milks	11689.322459	EUR	Hazardous
5	Stop using Kryptonite in production	2013-04-01 00:00:00.000	Dairy	Clark Kent	NULL	NULL	Moderate
6	Black and white logo paper	2012-06-01 00:00:00.000	Office supplies	Clark Kent	4880.199567	EUR	Simple
6	Black and white logo paper	2012-06-01 00:00:00.000	Office supplies	Clark Kent	4880.199567	EUR	Simple
```

To filter by project:

```
(venv) $ command.py ./test.txt --project=6
Project	Description	Start date	Category	Responsible	Savings amount	Currency	Complexity
6	Black and white logo paper	2012-06-01 00:00:00.000	Office supplies	Clark Kent	4880.199567	EUR	Simple
6	Black and white logo paper	2012-06-01 00:00:00.000	Office supplies	Clark Kent	4880.199567	EUR	Simple
```

To sort by start date:

```
(venv) $ command.py ./test.txt --sort-by-start-date
Project	Description	Start date	Category	Responsible	Savings amount	Currency	Complexity
6	Black and white logo paper	2012-06-01 00:00:00.000	Office supplies	Clark Kent	4880.199567	EUR	Simple
6	Black and white logo paper	2012-06-01 00:00:00.000	Office supplies	Clark Kent	4880.199567	EUR	Simple
3	Substitute Crème fraîche with evaporated milk in ice-cream products	2013-01-01 00:00:00.000	Dairy	Daisy Milks	141415.942696	EUR	Moderate
3	Substitute Crème fraîche with evaporated milk in ice-cream products	2013-01-01 00:00:00.000	Dairy	Daisy Milks	141415.942696	EUR	Moderate
4	Decrease production related non-categorized side costs	2013-01-01 00:00:00.000	Dairy	Daisy Milks	11689.322459	EUR	Hazardous
4	Decrease production related non-categorized side costs	2013-01-01 00:00:00.000	Dairy	Daisy Milks	11689.322459	EUR	Hazardous
5	Stop using Kryptonite in production	2013-04-01 00:00:00.000	Dairy	Clark Kent	NULL	NULL	Moderate
2	Harmonize Lactobacillus acidophilus sourcing	2014-01-01 00:00:00.000	Dairy	Daisy Milks	NULL	NULL	Simple
```

## Unit tests

To run the unit tests:

```
(venv) $ python setup.py test
```

## Architecture

For this application I've decided to use a pipeline of Python generator-based iterators - the advantage
of this is that it's super memory efficient because iterators can only be exhausted once, after which all
objects associated with the data transformation will be garbage collected.

The other advantage of using generators is that it allows me to split up each piece of transformation logic
into it's own tiny pipeline function, which

In terms of how much smaller this implementation could have been - probably a LOT smaller using generator expressions:

e.g.:

```
def split_lines(gen):
    """
    Iterate through a file stream generator and yield
    generator for each line, but without the newline char.

    :param generator gen:
    :return generator:
    """
    for line in gen:
        if line[-1] == "\n":
            yield line[:-1]
        else:
            yield line
```

could have been expressed as:

```
f = open('myfile.txt')
res = (line[:-1] if line[-1] == "\n" else line for line in f)
```

And so on for each generator pipeline. However, by implementing it as small def-style functions it becomes possible
to unit test individual parts of the pipeline, and it's more verbose/maintainable and understandable.

One remark worth mentioning is that I felt that I shouldn't spend a whole lot of time trying to implement some
generator-based sorting algo, so for the `--sort-by-start-date` option it exhausts the generator and sorts it in
a procedural manner.

### Libraries used

I've used the following libraries:

* **click** - Simple command-line application famework that deals with stuff like args and opts
* **pytest** - Most elegant, flexible and extensible (unit) test framework for Python

### Unit tests / test coverage

I've written unit tests with relatively high coverage of each generator function and each
validator, and the mechanism to ensure that even if you change the order of the columns, it
still applies the right validator configurations to the right column.

The tests are written in PyTest.

### Simple benchmarks

Command used:

```
time command.py ./test_(amount of rows).txt > /dev/null
```

With 15 rows:

```
real	0m0.229s
user	0m0.194s
sys	0m0.028s
```

With 1,000 rows:

```
real	0m0.235s
user	0m0.196s
sys	0m0.031s
```

With 10,000 rows:

```
real	0m0.331s
user	0m0.294s
sys	0m0.029s
```

With 100,000 rows:

```
real	0m1.323s
user	0m1.275s
sys	0m0.040s
```
### Potential improvements / known pitfalls

* Currently execution stops completely for rows where validation fails - it could be more robust by simply skipping them (pitfall)
* Due to the generator pipeline nature, it's tricky to validate each row's columns upfront because it prints each row at the end of
the pipeline, so you always see all rows printed before the row that fails validation and throws an error (pitfall / improvement)
* Performance will degrade once you toggle the `--sort-by-start-date` because `sorted()` exhausts the generator before sorting first (pitfall)
* A library like **terminaltables** can be used to draw result to the screen more pretty (improvement)
