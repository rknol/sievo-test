import click

from .configuration import (
    get_column_configuration,
    create_validators
)

from .generators import (
    split_lines,
    filter_comments,
    split_columns,
    validate_columns,
    filter_by_project,
)

from .utils import sort_by_column

@click.command()
@click.argument("file")
@click.option("--project", default=None, help="Filter results by project")
@click.option("--sort-by-start-date", is_flag=True, help="Sort results by start date")
def app(file, project, sort_by_start_date):
    with open(file) as f:
        """
        Step 1: Process the stream into data structures
        """
        res = split_lines(f)        # Split the file into lines without \n characters on the end
        res = filter_comments(res)  # Filter out empty lines + comment lines
        res = split_columns(res)    # Split each line into columns

        """
        Step 2: Ascertain the headers + get configuration
        """
        config = get_column_configuration()              # Retrieve the validator configuration
        headers = next(res)                              # Pull a list of headers out of the generator
        validators = create_validators(headers, config)  # Instantiate our set of validators for each header

        """
        Step 3: Validate each column against validation rules per header
        """
        res = validate_columns(res, validators)  # Validate each row's columns against the validators

        """
        Step 4: Optional filters
        """
        if project is not None:
            res = filter_by_project(res, headers, project)

        if sort_by_start_date:
            # We stop being a generator here because doing sorting iterating
            # over a generator step-by-step will take me a lot of time to ponder over.
            res = sort_by_column(res, headers.index("Start date"))

        """
        Step 4: Glue together all the lists and exhaust the generators to draw to the screen
        """
        print("\t".join(headers))                 # Print the headers on top
        [print("\t".join(line)) for line in res]  # Print each line of the results

if __name__ == '__main__':
    app()