import re


class Validator:
    def __init__(self, key):
        self.key = key

    def raise_error(self, msg, row):
        raise ValueError("\n\nRow: '{}'\nError: Column '{}' {}\n".format(" ".join(row), self.key, msg))

    def __repr__(self):
        return "<Validator '{}'>".format(self.key)


class Required(Validator):
    def validate(self, val, row):
        if val == 'NULL':
            self.raise_error("cannot be NULL.", row)


class Date(Validator):
    def validate(self, val, row):
        pattern = re.compile("^([0-9]{4}-[0-9]{2}-[0-9]{2}\s{1}[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3})+$")
        if not pattern.match(val):
            self.raise_error("date format needs to be 'YYYY-mm-dd HH:ii:ss.vvv'", row)


class Money(Validator):
    def validate(self, val, row):
        try:
            if val != "NULL":
                val = float(val)
        except:
            self.raise_error("should be a floating point number.", row)


class Complexity(Validator):
    def validate(self, val, row):
        if val not in ["Simple", "Moderate", "Hazardous"]:
            self.raise_error("should be either 'Simple', 'Moderate' or 'Hazardous'.", row)
