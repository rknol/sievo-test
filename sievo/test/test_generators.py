from sievo.generators import (
    split_lines,
    split_columns,
    filter_comments,
)


def test_split_lines_normal_lines():
    lines = ["test\n", "test2\n", "test3"]
    res = split_lines(lines)
    new = [new_line for new_line in res]  # Exhaust the generator

    assert len(new) == 3  # Assert that we will end up with actually 3 rows
    assert new[0][-1] != "\n"  # Assert that we chop off newlines
    assert len(new[2]) == 5  # Assert that we don't chop off anything except newlines


def test_filter_comments():
    lines = ["test", "test2", "# test3"]
    res = filter_comments(lines)
    new = [new_line for new_line in res]  # Exhaust the generator

    assert len(new) == 2  # Assert that all comments are filtered


def test_filter_comments_empty_line():
    lines = ["test", "", "# test3"]
    res = filter_comments(lines)
    new = [new_line for new_line in res]  # Exhaust the generator

    assert len(new) == 1  # Assert that all empty lines AND comments are filtered


def test_split_columns():
    lines = [
        "test\ttest2\ttest3",
        "test4\ttest5\ttest6"
    ]
    res = split_columns(lines)
    new = [new_line for new_line in res]  # Exhaust the generator

    assert len(new[0]) == 3  # Assert that we will actually end up with 3 cols
    assert new[0][0] == "test"  # Assert the expected value for a specific column
