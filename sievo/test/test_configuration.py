from sievo.validators import Required, Money, Validator
from sievo.configuration import create_validators


def test_create_validators():
    headers = [
        "Header 1",
        "Header 2"
    ]

    conf = {
        "Header 1": [Required],
        "Header 2": [Required, Money]
    }

    validators = create_validators(headers, conf)

    assert len(validators[0]) == 1  # Assert that our first column has just a single validator
    assert len(validators[1]) == 2  # Assert that our second column has two validators
    assert isinstance(validators[0][0], Validator)  # Assert base type of the validator
    assert isinstance(validators[0][0], Required)  # Assert specific type of the validator


def test_create_validators_out_of_order():
    """
    This is the exact same test scenario as above but confirms that the list
    of headers doesn't have to be in a very specific order
    """
    headers = [
        "Header 2",
        "Header 1"
    ]

    conf = {
        "Header 1": [Required],
        "Header 2": [Required, Money]
    }

    validators = create_validators(headers, conf)

    assert len(validators[0]) == 2  # Assert that our first column has just a single validator
    assert len(validators[1]) == 1  # Assert that our second column has two validators
    assert isinstance(validators[1][0], Validator)  # Assert base type of the validator
    assert isinstance(validators[1][0], Required)  # Assert specific type of the validator