import pytest


from sievo.validators import (
    Required,
    Date,
    Money,
    Complexity
)


def test_complexity_success():
    val = Complexity("Example")

    try:
        val.validate("Simple", ["Simple", "Other", "Bla"])
        val.validate("Moderate", ["Moderate", "Other", "Bla"])
        val.validate("Hazardous", ["Hazardous", "Other", "Bla"])
    except:
        raise Exception("Failed asserting that Complexity validator passes valid values.")


def test_complexity_fail():
    val = Complexity("Example")

    with pytest.raises(ValueError):
        val.validate("False", ["False", "Other", "Bla"])


def test_required_success():
    val = Required("Example")

    try:
        val.validate("Not blank", ["Not blank", "Other", "Bla"])
    except:
        raise Exception("Failed asserting that Required validator passes valid values.")


def test_required_fail():
    val = Complexity("Example")

    with pytest.raises(ValueError):
        val.validate("NULL", ["NULL", "Other", "Bla"])


def test_money_success():
    val = Money("Example")

    try:
        val.validate("3.14", ["3.14", "Other", "Bla"])
        val.validate("1.12", ["1.12", "Other", "Bla"])
        val.validate("332948.234234", ["332948.234234", "Other", "Bla"])
        val.validate("NULL", ["NULL", "Other", "Bla"])
    except:
        raise Exception("Failed asserting that Money validator passes valid values.")


def test_money_fail():
    val = Money("Example")

    with pytest.raises(ValueError):
        val.validate("asdadasd", ["3", "Other", "Bla"])


def test_date_success():
    val = Date("Example")

    try:
        val.validate("2012-06-01 00:00:00.000", ["2012-06-01 00:00:00.000", "Other", "Bla"])
    except:
        raise Exception("Failed asserting that Date validator passes valid values.")

def test_date_fail():
    val = Date("Example")

    with pytest.raises(ValueError):
        val.validate("asdadasd", ["3", "Other", "Bla"])

    with pytest.raises(ValueError):
        val.validate("2012-06-01 00:00:00", ["3", "Other", "Bla"])

    with pytest.raises(ValueError):
        val.validate("2012-06-01", ["3", "Other", "Bla"])