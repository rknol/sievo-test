def sort_by_column(gen, column):
    return sorted(gen, key=lambda columns: columns[column])
