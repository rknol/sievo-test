def split_lines(gen):
    """
    Iterate through a file stream generator and yield
    generator for each line, but without the newline char.

    :param generator gen:
    :return generator:
    """
    for line in gen:
        if line[-1] == "\n":
            yield line[:-1]
        else:
            yield line


def filter_comments(gen):
    """
    Iterate through each line in the stream generator
    and only yield generators for lines that shouldn't
    be filtered.

    :param generator gen:
    :return generator:
    """
    for line in gen:
        if len(line) > 0 and line[0] != '#':
            yield line


def split_columns(gen):
    """
    Split each line inside the stream generator into
    a list of columns

    :param generator gen:
    :return generator:
    """
    for line in gen:
        yield line.split("\t")


def validate_columns(gen, validators):
    """
    Run each column within each row through each of the
    assigned validators

    :param generator gen:
    :param list headers:
    :param dict config:
    :return generator:
    """
    for i, row in enumerate(gen):
        for i2, col in enumerate(row):
            [validator.validate(col, row) for validator in validators[i2]]
        yield row


def filter_by_project(gen, headers, project):
    for i, row in enumerate(gen):
        if row[headers.index('Project')] == project:
            yield row
