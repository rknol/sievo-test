from sievo.validators import (
    Required,
    Money,
    Complexity,
    Date
)


def get_column_configuration():
    """
    We define a configuration of which validators
    apply to which columns

    :return dict:
    """
    return {
        'Project': [Required],
        'Description': [Required],
        'Start date': [Required, Date],
        'Category': [Required],
        'Responsible': [Required],
        'Savings amount': [Money],
        'Currency': [],
        'Complexity': [Complexity]
    }


def create_validators(headers, configuration):
    """
    List comprehension to instantiate each validator configured
    in the order that the headers came in.

    :param list headers:
    :param dict configuration:
    :return list:
    """
    return [[validator(header) for validator in configuration[header]] for header in headers]
